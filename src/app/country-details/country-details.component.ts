import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { CountryStatisticsService } from '../country-statistics.service';

// interfaces
import { Country } from '../country';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss'],
})
export class CountryDetailsComponent implements OnInit {
  country: Country | undefined;
  countryName: string | undefined;
  JSON;

  constructor(
    private route: ActivatedRoute,
    private countryStatisticService: CountryStatisticsService
  ) {
    this.JSON = JSON;
  }

  ngOnInit(): void {
    // get the country name from the route path
    const routeParams = this.route.snapshot.paramMap;
    const countryName = routeParams.get('countryName') || '';
    this.countryName = countryName;

    this.countryStatisticService
      .getCountryStatistic(this.countryName)
      .subscribe({
        next: (data: any) => {
          console.log('data for country => ', this.countryName, ' => ', data);
          this.country = data.response[0];
        },
      });
  }
}
