import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { CountryListComponent } from './country-list.component';
import { Country } from '../country';
import { By } from '@angular/platform-browser';

describe('CountryListComponent', () => {
  let component: CountryListComponent;
  let fixture: ComponentFixture<CountryListComponent>;

  let httpClient: HttpClient;

  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  const mockedCountries: Country[] = [
    {
      cases: {
        '1M_pop': '58',
        active: 73,
        critical: 3,
        new: '+5',
        recovered: 78315,
        total: 83022,
      },
      continent: 'Asia',
      country: 'China',
      day: '2020-06-02',
      deaths: {
        '1M_pop': '3',
        new: null,
        total: 4634,
      },
      population: 1439323776,
      tests: {
        '1M_pop': '',
        total: 0,
      },
      time: '2020-06-02T20:45:06+00:00',
    },
    {
      cases: {
        '1M_pop': '3862',
        active: 39893,
        critical: 408,
        new: '+318',
        recovered: 160092,
        total: 233515,
      },
      continent: 'Europe',
      country: 'Italy',
      day: '2020-06-02',
      deaths: {
        '1M_pop': '555',
        new: '+55',
        total: 33530,
      },
      population: 60468537,
      tests: {
        '1M_pop': '65527',
        total: 3962292,
      },
      time: '2020-06-02T20:45:06+00:00',
    },
    {
      cases: {
        '1M_pop': '6139',
        active: 0,
        critical: 617,
        new: '+294',
        recovered: 0,
        total: 287012,
      },
      continent: 'Europe',
      country: 'Spain',
      day: '2020-06-02',
      deaths: {
        '1M_pop': '580',
        new: null,
        total: 27127,
      },
      population: 46753394,
      tests: {
        '1M_pop': '86921',
        total: 4063843,
      },
      time: '2020-06-02T20:45:06+00:00',
    },
    {
      cases: {
        '1M_pop': '5665',
        active: 1146293,
        critical: 16939,
        new: '+14945',
        recovered: 620129,
        total: 1874268,
      },
      continent: 'North-America',
      country: 'USA',
      day: '2020-06-02',
      deaths: {
        '1M_pop': '326',
        new: '+921',
        total: 107846,
      },
      population: 330848770,
      tests: {
        '1M_pop': '55745',
        total: 18443243,
      },
      time: '2020-06-02T20:45:06+00:00',
    },
  ];
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [CountryListComponent],
    });

    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);

    fixture = TestBed.createComponent(CountryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', async () => {
    httpClientSpy.get.and.returnValue(of({ response: mockedCountries }));
    expect(component).toBeTruthy();

    const element = fixture.debugElement;
    const title = element.query(By.css('h1')).nativeElement;

    expect(title.innerHTML).toContain('Countries statistic');
    // TODO: add test for the loading table
    // const table = element.query(By.css('table')).nativeElement;
    // expect(table).toBeTruthy();

    // expect(httpClientSpy.get.calls.count()).toBe(1);
    // let tableRows = fixture.nativeElement.querySelectorAll('tr');
    // expect(tableRows.length).toBe(4);
  });
});
