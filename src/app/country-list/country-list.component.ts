import { Component, OnInit } from '@angular/core';

import { CountryStatisticsService } from '../country-statistics.service';

import { Country } from '../country';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss'],
})
export class CountryListComponent implements OnInit {
  countries: Country[] = [];

  ngOnInit(): void {
    this.countryStatisticService.getCountries().subscribe({
      next: (data: any) => {
        console.log(data);
        this.countries = data.response;
      },
      error: (e) => console.log(e),
    });
  }

  constructor(private countryStatisticService: CountryStatisticsService) {}
}
