import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { CountryStatisticsService } from './country-statistics.service';

describe('CountryStatisticsService', () => {
  let service: CountryStatisticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(CountryStatisticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
