import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../environments/environment';

// interfaces
import { Country } from './country';

@Injectable({
  providedIn: 'root',
})
export class CountryStatisticsService {
  url = 'https://covid-193.p.rapidapi.com/statistics';

  // to make http client available
  constructor(private http: HttpClient) {}

  getCountries(): Observable<{ response: Country[] }> {
    return this.http.get<{ response: Country[] }>(this.url, {
      headers: {
        'X-RapidAPI-Key': environment.RAPID_API_KEY,
        'X-RapidAPI-Host': environment.RAPID_API_HOST,
      },
    });
  }

  getCountryStatistic(name: string) {
    return this.http.get<{ response: Country[] }>(this.url, {
      headers: {
        'X-RapidAPI-Key': environment.RAPID_API_KEY,
        'X-RapidAPI-Host': environment.RAPID_API_HOST,
      },
      params: {
        country: name,
      },
    });
  }
}
