export interface Country {
  continent: string;
  country: string;
  population: number;
  day: string;
  time: string;
  cases: {
    new: number | string;
    active: number;
    critical: number;
    recovered: number;
    '1M_pop': string;
    total: number;
  };
  deaths: {
    new: string | null;
    '1M_pop': string;
    total: number;
  };
  tests: {
    '1M_pop': string;
    total: number;
  };
}
